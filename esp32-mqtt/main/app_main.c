#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_wpa2.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"
#include "tcpip_adapter.h"
#include "esp_smartconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt.h"

#include "driver/gpio.h"

#undef CONFIG_MQTT_SECURITY_ON
const char *MQTT_TAG = "MQTT_SAMPLE";
static EventGroupHandle_t wifi_event_group;
#define GPIO_INPUT_IO_0     17
#define GPIO_INPUT_IO_1     16
#define GPIO_INPUT_IO_2     18
#define GPIO_INPUT_IO_3     19
#define GPIO_INPUT_IO_4     21
#define GPIO_INPUT_IO_5     12
#define GPIO_INPUT_IO_6     27
#define GPIO_INPUT_IO_7     4
#define GPIO_INPUT_PIN_SEL  ((1ULL<<GPIO_INPUT_IO_0) | (1ULL<<GPIO_INPUT_IO_1)| (1ULL<<GPIO_INPUT_IO_2)| (1ULL<<GPIO_INPUT_IO_3)| (1ULL<<GPIO_INPUT_IO_4)| (1ULL<<GPIO_INPUT_IO_5)| (1ULL<<GPIO_INPUT_IO_6)| (1ULL<<GPIO_INPUT_IO_7))
#define ESP_INTR_FLAG_DEFAULT 0
/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
TaskHandle_t wificount_handle = NULL;
TaskHandle_t smartconfig_example_task_handle = NULL;
BaseType_t xReturned=NULL;
BaseType_t xReturned_sc=NULL;
static const int CONNECTED_BIT = BIT0;
static const int ESPTOUCH_DONE_BIT = BIT1;
static const char *TAG = "sc";
void smartconfig_example_task(void * parm);
void wificount(void * parm);
void mqtt_task_create(void * parm);
void connected_cb(void *self, void *params)
{
    mqtt_client *client = (mqtt_client *)self;
    char buffer[1];
    for(uint8_t i=0;i<=2;i++)
    {
        sprintf(buffer,"%d",gpio_get_level(17));
        printf("pin17: %s\n",buffer);
        mqtt_publish(client, "m001_long", buffer , 1, 0, 0);
        vTaskDelay(1000);
    }
    sprintf(buffer,"%d",gpio_get_level(16));
    printf("pin16: %s\n",buffer);
    mqtt_publish(client, "m001_line1", buffer , 1, 0, 0);
    sprintf(buffer,"%d",gpio_get_level(18));
    printf("pin18: %s\n",buffer);
    mqtt_publish(client, "m001_line2", buffer , 1, 0, 0);
    sprintf(buffer,"%d",gpio_get_level(19));
    printf("pin19: %s\n",buffer);
    mqtt_publish(client, "m001_line3", buffer , 1, 0, 0);
    sprintf(buffer,"%d",gpio_get_level(21));
    printf("pin21: %s\n",buffer);
    mqtt_publish(client, "m001_line4", buffer , 1, 0, 0);
    sprintf(buffer,"%d",gpio_get_level(12));
    printf("pin12: %s\n",buffer);
    mqtt_publish(client, "m001_line5", buffer , 1, 0, 0);
    sprintf(buffer,"%d",gpio_get_level(27));
    printf("pin27: %s\n",buffer);
    mqtt_publish(client, "m001_line6", buffer , 1, 0, 0);
    sprintf(buffer,"%d",gpio_get_level(4));
    printf("pin4: %s\n",buffer);
    mqtt_publish(client, "m001_power", buffer , 1, 0, 0);
    printf("Done!!\n");
    mqtt_stop();
}
void disconnected_cb(void *self, void *params)
{
    vTaskDelay(2000);
    xTaskCreate(mqtt_task_create, "mqtt_task_create", 4096, NULL, 3, NULL);
}

/*void reconnect_cb(void *self, void *params)
{

}
void subscribe_cb(void *self, void *params)
{
    ESP_LOGI(MQTT_TAG, "[APP] Subscribe ok, test publish msg");
    mqtt_client *client = (mqtt_client *)self;
    mqtt_publish(client, "mqtt_esp32", "abcde", 5, 0, 0);
}

void publish_cb(void *self, void *params)
{
}
void data_cb(void *self, void *params)
{
    mqtt_client *client = (mqtt_client *)self;
    mqtt_event_data_t *event_data = (mqtt_event_data_t *)params;

    if(event_data->data_offset == 0) {

        char *topic = malloc(event_data->topic_length + 1);
        memcpy(topic, event_data->topic, event_data->topic_length);
        topic[event_data->topic_length] = 0;
        ESP_LOGI(MQTT_TAG, "[APP] Publish topic: %s", topic);
        free(topic);
    }

    // char *data = malloc(event_data->data_length + 1);
    // memcpy(data, event_data->data, event_data->data_length);
    // data[event_data->data_length] = 0;
    ESP_LOGI(MQTT_TAG, "[APP] Publish data[%d/%d bytes]",
             event_data->data_length + event_data->data_offset,
             event_data->data_total_length);
    // data);

    // free(data);

}
*/
 mqtt_settings settings = {
    .host = "54.169.75.126",  // or domain, ex: "google.com",
    //.host = "192.168.3.67",
    .port = 1883,
    .client_id = "m001", 
    .username = "m001",
    .password = "m001",
    .clean_session = 0, 
    .keepalive = 15, //second
    .lwt_topic = "/lwt",    // = "" for disable lwt, will don't care other options
    .lwt_msg = "offline",
    .lwt_qos = 0,
    .lwt_retain = 0,
    .connected_cb = connected_cb,
    .disconnected_cb = disconnected_cb
};



static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
        case SYSTEM_EVENT_STA_START:
            //xTaskCreate(smartconfig_example_task, "smartconfig_example_task", 4096, NULL, 3, NULL);
            esp_wifi_connect();
            xReturned=xTaskCreate(wificount, "wificount", 4096, 1, 3, &wificount_handle);
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            //init app here
            if( wificount_handle != NULL )
                {
                    vTaskDelete(wificount_handle);
                }
            xTaskCreate(mqtt_task_create, "mqtt_task_create", 4096, NULL, 3, NULL);     
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            /* This is a workaround as ESP32 WiFi libs don't currently
               auto-reassociate. */
            esp_wifi_connect();
            if( wificount_handle == NULL )
                {
                    xTaskCreate(wificount, "wificount", 4096, 1, 3, &wificount_handle);
                }
            mqtt_stop();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            break;
        default:
            break;
    }
    return ESP_OK;
}
    wifi_config_t *wifi_config;
static void wifi_conn_init(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    //ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_LOGI(MQTT_TAG, "start the WIFI \n");
    ESP_ERROR_CHECK(esp_wifi_start());
}
static void sc_callback(smartconfig_status_t status, void *pdata)
{
    switch (status) {
        case SC_STATUS_WAIT:
            ESP_LOGI(TAG, "SC_STATUS_WAIT");
            break;
        case SC_STATUS_FIND_CHANNEL:
            ESP_LOGI(TAG, "SC_STATUS_FINDING_CHANNEL");
            break;
        case SC_STATUS_GETTING_SSID_PSWD:
            ESP_LOGI(TAG, "SC_STATUS_GETTING_SSID_PSWD");
            break;
        case SC_STATUS_LINK:
            ESP_LOGI(TAG, "SC_STATUS_LINK");
            wifi_config = pdata;
            ESP_LOGI(TAG, "SSID:%s", wifi_config->sta.ssid);
            ESP_LOGI(TAG, "PASSWORD:%s", wifi_config->sta.password);
            ESP_ERROR_CHECK( esp_wifi_disconnect() );
            ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));
            ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, wifi_config) );
            esp_wifi_set_auto_connect(true);
            ESP_ERROR_CHECK( esp_wifi_connect() );
            break;
        case SC_STATUS_LINK_OVER:
            ESP_LOGI(TAG, "SC_STATUS_LINK_OVER");
            if (pdata != NULL) {
                uint8_t phone_ip[4] = { 0 };
                memcpy(phone_ip, (uint8_t* )pdata, 4);
                ESP_LOGI(TAG, "Phone ip: %d.%d.%d.%d\n", phone_ip[0], phone_ip[1], phone_ip[2], phone_ip[3]);
            }
            xEventGroupSetBits(wifi_event_group, ESPTOUCH_DONE_BIT);
            break;
        default:
            break;
    }
}
void mqtt_task_create(void * parm)
{
    mqtt_start(&settings);
    vTaskDelete(NULL);
}
void wificount(void * parm)
{
    configASSERT( ( ( uint32_t ) parm ) == 1 );
    vTaskDelay(10000);
    if(smartconfig_example_task_handle == NULL)
    {
        xReturned_sc=xTaskCreate(smartconfig_example_task, "smartconfig_example_task", 4096, 1, 3, &smartconfig_example_task_handle);
    }
    vTaskDelete(NULL);
}
void smartconfig_example_task(void * parm)
{
    wificount_handle = NULL;
    configASSERT( ( ( uint32_t ) parm ) == 1 );
    EventBits_t uxBits;
    ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
    ESP_ERROR_CHECK( esp_smartconfig_start(sc_callback) );
    wificount_handle = NULL;
    while (1) {
        uxBits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY); 
        if(uxBits & CONNECTED_BIT) {
            ESP_LOGI(TAG, "WiFi Connected to ap");
        }
        if(uxBits & ESPTOUCH_DONE_BIT) {
            ESP_LOGI(TAG, "smartconfig over");
            esp_smartconfig_stop();
            vTaskDelete(NULL);
        }
    }
}
void app_main()
{
    nvs_flash_init();

    gpio_config_t io_conf;
    //interrupt of rising edge
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //bit mask of the pins, use GPIO4/5 here
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    //set as input mode    
    io_conf.mode = GPIO_MODE_INPUT;
    //disable pull-down mode
    io_conf.pull_down_en = 1;
    //enable pull-up mode
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);


    wifi_conn_init();
}
